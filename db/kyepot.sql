-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: kyepot
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `kyepot`
--

/*!40000 DROP DATABASE IF EXISTS `kyepot`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `kyepot` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kyepot`;

--
-- Table structure for table `kp_customer_detail`
--

DROP TABLE IF EXISTS `kp_customer_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kp_customer_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gu_id` varchar(512) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `added_by_id` bigint(20) NOT NULL,
  `upd_by_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `account_no` bigint(20) NOT NULL,
  `address` varchar(4000) NOT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `obj_status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gu_id` (`gu_id`),
  UNIQUE KEY `account_no` (`account_no`),
  KEY `added_by_id` (`added_by_id`),
  KEY `upd_by_id` (`upd_by_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `KPCustomerDetail_ADDED_BY_ID` FOREIGN KEY (`added_by_id`) REFERENCES `kp_user` (`id`),
  CONSTRAINT `KPCustomerDetail_UPD_BY_ID` FOREIGN KEY (`upd_by_id`) REFERENCES `kp_user` (`id`),
  CONSTRAINT `KPCustomerDetail_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `kp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kp_customer_detail`
--

LOCK TABLES `kp_customer_detail` WRITE;
/*!40000 ALTER TABLE `kp_customer_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `kp_customer_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kp_transaction`
--

DROP TABLE IF EXISTS `kp_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kp_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gu_id` varchar(512) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `added_by_id` bigint(20) NOT NULL,
  `upd_by_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `transaction_type` int(11) NOT NULL,
  `transaction_mode` int(11) NOT NULL,
  `transaction_amt` double NOT NULL,
  `closing_balance` double NOT NULL,
  `obj_status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gu_id` (`gu_id`),
  KEY `added_by_id` (`added_by_id`),
  KEY `upd_by_id` (`upd_by_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `KPTransaction_ADDED_BY_ID` FOREIGN KEY (`added_by_id`) REFERENCES `kp_user` (`id`),
  CONSTRAINT `KPTransaction_UPD_BY_ID` FOREIGN KEY (`upd_by_id`) REFERENCES `kp_user` (`id`),
  CONSTRAINT `KPTransaction_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `kp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kp_transaction`
--

LOCK TABLES `kp_transaction` WRITE;
/*!40000 ALTER TABLE `kp_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `kp_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kp_user`
--

DROP TABLE IF EXISTS `kp_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kp_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gu_id` varchar(512) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `email_id` varchar(1024) NOT NULL,
  `password` varchar(1024) DEFAULT NULL,
  `obj_status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gu_id` (`gu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kp_user`
--

LOCK TABLES `kp_user` WRITE;
/*!40000 ALTER TABLE `kp_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `kp_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kp_user_role`
--

DROP TABLE IF EXISTS `kp_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kp_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gu_id` varchar(512) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_role` varchar(64) NOT NULL,
  `obj_status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gu_id` (`gu_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `KPUserRole_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `kp_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kp_user_role`
--

LOCK TABLES `kp_user_role` WRITE;
/*!40000 ALTER TABLE `kp_user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `kp_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-13 16:20:14
