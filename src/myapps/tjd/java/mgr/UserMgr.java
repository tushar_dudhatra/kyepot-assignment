/**
 * 
 */
package myapps.tjd.java.mgr;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import myapps.tjd.common.KPConstants;
import myapps.tjd.db.dao.KPDaoHandler;
import myapps.tjd.db.entity.KPCustomerDetail;
import myapps.tjd.db.entity.KPUser;
import myapps.tjd.java.model.VCustomerDetail;
import myapps.tjd.java.model.VUser;
import myapps.tjd.java.model.VUserCredentials;
import myapps.tjd.java.model.VUserRole;
import myapps.tjd.java.service.CustomerDetailService;
import myapps.tjd.java.service.UserRoleService;
import myapps.tjd.java.service.UserService;

/**
 * @author tdudhatra
 *
 */
@Component
public class UserMgr {

	@Autowired
	KPDaoHandler			daoHandler;

	@Autowired
	UserService				userService;

	@Autowired
	CustomerDetailService	customerDetailService;

	@Autowired
	UserRoleService			userRoleService;

	public VUser createUser(VUser vUser) throws Exception {

		try {

			if (vUser == null) {
				throw new Exception();
			}

			VCustomerDetail vCustomerDetail = vUser.getCustomerDetail();
			List<String> roles = vUser.getUserRoles();

			this.validateUserToCreate(vUser);
			vCustomerDetail = this.validateCustomerDetailAndCreateAccountNo(vCustomerDetail);

			KPUser user = daoHandler.getKPUser().findByEmailIdAndAccountNo(vUser.getEmailId(), vCustomerDetail.getAccountNo());
			if (user != null) {
				throw new Exception("Caught Unknown Error while creating user !");
			}

			vUser = userService.createObject(vUser);

			vCustomerDetail.setUserId(vUser.getId());
			vCustomerDetail = customerDetailService.createObject(vCustomerDetail);

			for (String role : roles) {
				VUserRole userRole = new VUserRole();
				userRole.setUserId(vUser.getId());
				userRole.setUserRole(role);
				userRoleService.createObject(userRole);
			}
			vUser = this.getUserDetailById(vUser.getId());
		} catch (Exception e) {
			throw new Exception("Caught Unknown Error while creating user !");
		}

		return vUser;
	}

	private VCustomerDetail validateCustomerDetailAndCreateAccountNo(VCustomerDetail vCustomerDetail) throws Exception {

		if (StringUtils.isEmpty(vCustomerDetail.getAddress())) {
			throw new Exception("Address is mandatory field !");
		}

		if (StringUtils.isEmpty(vCustomerDetail.getContactNo())) {
			throw new Exception("Contact No is mandatory field !");
		}

		Long newAcNo = generateNextAccountNumber();
		vCustomerDetail.setAccountNo(newAcNo);

		return vCustomerDetail;

	}

	private Long generateNextAccountNumber() {

		Long accountNo = null;

		KPCustomerDetail lastCustomer = daoHandler.getKPCustomerDetail().findLastCreatedCustomer();

		if (lastCustomer == null) {
			accountNo = KPConstants.ACCOUNT_NO_STARTING_POINT;
		} else {
			accountNo = lastCustomer.getAccountNo() + 10;
		}

		return accountNo;
	}

	private void validateUserToCreate(VUser vUser) throws Exception {

		if (StringUtils.isEmpty(vUser.getFirstName())) {
			throw new Exception("FirstName is mandatory field !");
		}

		if (StringUtils.isEmpty(vUser.getLastName())) {
			throw new Exception("LastName is mandatory field !");
		}

		if (StringUtils.isEmpty(vUser.getEmailId())) {
			throw new Exception("EmailId is mandatory field !");
		}

		List<String> userRoles = vUser.getUserRoles();

		if (CollectionUtils.isEmpty(userRoles)) {
			throw new Exception("UserRoles cannot be empty !");
		}

		if (userRoles.size() != 1) {
			throw new Exception("Invalid Values of UserRole");
		}

		if (!StringUtils.equalsIgnoreCase(userRoles.get(0), "ROLE_USER")) {
			throw new Exception("Invalid Values of UserRole");
		}
	}

	public VUser getUserDetailById(Long userId) throws Exception {

		if (userId == null) {
			throw new Exception("UserId can not be null !");
		}
		return userService.readObject(userId);
	}

	public VUser getUserDetailByAccountNoAndEmailId(Long accountNo, String emailId) throws Exception {

		if (StringUtils.isEmpty(emailId) || accountNo == null) {
			throw new Exception("To View UserDetails, EmailId and AccountNo are the mandatory !");
		}

		KPUser user = daoHandler.getKPUser().findByEmailIdAndAccountNo(emailId, accountNo);

		if (user == null) {
			throw new Exception("No User Found with EmailId: [" + emailId + "] and AccountNo: [" + accountNo + "] !");
		}

		return userService.getPopulatedViewObject(user);
	}

	public void authenticate(VUserCredentials vUserCredentials) throws Exception {
		// TODO Auto-generated method stub

	}

}
