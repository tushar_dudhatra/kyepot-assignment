package myapps.tjd.java.mgr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import myapps.tjd.common.KPConstants;
import myapps.tjd.db.dao.KPDaoHandler;
import myapps.tjd.db.entity.KPTransaction;
import myapps.tjd.java.model.VTransaction;
import myapps.tjd.java.service.TransactionService;

/**
 * @author tdudhatra
 *
 */

@Component
public class TransactionMgr {

	@Autowired
	KPDaoHandler		daoHandler;

	@Autowired
	TransactionService	transactionService;

	public VTransaction doTransaction(VTransaction vTransaction) throws Exception {

		Long userId = vTransaction.getUserId();
		int trxType = vTransaction.getTransactionType();
		int trxMode = vTransaction.getTransactionMode();
		double trxAmt = vTransaction.getTransactionAmt();

		if (userId == null) {
			throw new Exception("UserId cannot be null !");
		}

		if (trxAmt <= 0) {
			throw new Exception("Transaction amount can not be less than or equal to zero !");
		}

		if (trxMode != KPConstants.TRANSACTION_MODE_CASH && trxMode != KPConstants.TRANSACTION_MODE_CHEQUE) {
			throw new Exception("Invalid value of TransactionMode !");
		}

		KPTransaction lastTrx = daoHandler.getKPTransaction().findLastTransactionOfUser(vTransaction.getUserId());
		if (trxType == KPConstants.TRANSACTION_TYPE_DEBIT) {

			if (lastTrx == null) {
				throw new Exception("User does not have sufficient balance to make this transaction !");
			}

			double closingBal = lastTrx.getClosingBalance();

			if (trxAmt > closingBal) {
				throw new Exception("User does not have sufficient balance to make this transaction !");
			}

			vTransaction.setClosingBalance(closingBal - trxAmt);

			vTransaction = transactionService.createObject(vTransaction);

		} else if (trxType == KPConstants.TRANSACTION_TYPE_CREDIT) {

			double closingBal = 0;
			if (lastTrx != null) {
				closingBal = lastTrx.getClosingBalance() + trxAmt;
			} else {
				closingBal = trxAmt;
			}

			vTransaction.setClosingBalance(closingBal);
			vTransaction = transactionService.createObject(vTransaction);

		} else {
			throw new Exception("Invalid Transaction Type !");
		}

		return vTransaction;
	}

}
