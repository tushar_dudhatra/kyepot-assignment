/**
 * 
 */
package myapps.tjd.java.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author tdudhatra
 *
 */

@XmlRootElement
public class VUserCredentials {

	protected String	userEmail;

	protected String	password;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
