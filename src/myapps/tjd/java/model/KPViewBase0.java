package myapps.tjd.java.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author tdudhatra
 *
 */
@XmlRootElement
public class KPViewBase0 implements Serializable {
	private static final long	serialVersionUID	= 1L;

	private Long				id					= null;
	private String				guid				= null;
	private Date				createTime			= null;
	private Date				updateTime			= null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
