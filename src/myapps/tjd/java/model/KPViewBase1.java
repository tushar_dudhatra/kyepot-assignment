package myapps.tjd.java.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author tdudhatra
 *
 */
@XmlRootElement
public class KPViewBase1 extends KPViewBase0 implements Serializable {
	private static final long	serialVersionUID	= 1L;

	private String				createdBy			= null;
	private String				updatedBy			= null;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
