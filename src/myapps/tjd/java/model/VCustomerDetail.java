/**
 * 
 */
package myapps.tjd.java.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import myapps.tjd.common.KPConstants;

/**
 * @author tdudhatra
 *
 */

@XmlRootElement
public class VCustomerDetail extends KPViewBase1 implements Serializable {
	private static final long	serialVersionUID	= 1L;

	protected Long				userId;

	protected Long				accountNo;

	protected String			address;

	protected String			contactNo;

	protected double			closingBalance;

	protected Date				closingBalanceAsOfDate;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(Long accountNo) {
		this.accountNo = accountNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public double getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(double closingBalance) {
		this.closingBalance = closingBalance;
	}

	public Date getClosingBalanceAsOfDate() {
		return closingBalanceAsOfDate;
	}

	public void setClosingBalanceAsOfDate(Date closingBalanceAsOfDate) {
		this.closingBalanceAsOfDate = closingBalanceAsOfDate;
	}

	public Integer getMyClassType() {
		return KPConstants.CLASS_TYPE_CUSTOMER_DETAIL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNo == null) ? 0 : accountNo.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		long temp;
		temp = Double.doubleToLongBits(closingBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((closingBalanceAsOfDate == null) ? 0 : closingBalanceAsOfDate.hashCode());
		result = prime * result + ((contactNo == null) ? 0 : contactNo.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VCustomerDetail other = (VCustomerDetail) obj;
		if (accountNo == null) {
			if (other.accountNo != null)
				return false;
		} else if (!accountNo.equals(other.accountNo))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (Double.doubleToLongBits(closingBalance) != Double.doubleToLongBits(other.closingBalance))
			return false;
		if (closingBalanceAsOfDate == null) {
			if (other.closingBalanceAsOfDate != null)
				return false;
		} else if (!closingBalanceAsOfDate.equals(other.closingBalanceAsOfDate))
			return false;
		if (contactNo == null) {
			if (other.contactNo != null)
				return false;
		} else if (!contactNo.equals(other.contactNo))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
