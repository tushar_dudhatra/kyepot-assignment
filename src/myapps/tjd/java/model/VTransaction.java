/**
 * 
 */
package myapps.tjd.java.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import myapps.tjd.common.KPConstants;

/**
 * @author tdudhatra
 *
 */
@XmlRootElement
public class VTransaction extends KPViewBase1 implements Serializable {
	private static final long	serialVersionUID	= 1L;

	protected Long				userId;

	protected int				transactionType;

	protected int				transactionMode;

	protected double			transactionAmt;

	protected double			closingBalance;

	protected VUserCredentials	userCredentials;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public int getTransactionMode() {
		return transactionMode;
	}

	public void setTransactionMode(int transactionMode) {
		this.transactionMode = transactionMode;
	}

	public double getTransactionAmt() {
		return transactionAmt;
	}

	public void setTransactionAmt(double transactionAmt) {
		this.transactionAmt = transactionAmt;
	}

	public double getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(double closingBalance) {
		this.closingBalance = closingBalance;
	}

	public VUserCredentials getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(VUserCredentials userCredentials) {
		this.userCredentials = userCredentials;
	}

	public Integer getMyClassType() {
		return KPConstants.CLASS_TYPE_TRANSACTION;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(closingBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(transactionAmt);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + transactionMode;
		result = prime * result + transactionType;
		result = prime * result + ((userCredentials == null) ? 0 : userCredentials.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VTransaction other = (VTransaction) obj;
		if (Double.doubleToLongBits(closingBalance) != Double.doubleToLongBits(other.closingBalance))
			return false;
		if (Double.doubleToLongBits(transactionAmt) != Double.doubleToLongBits(other.transactionAmt))
			return false;
		if (transactionMode != other.transactionMode)
			return false;
		if (transactionType != other.transactionType)
			return false;
		if (userCredentials == null) {
			if (other.userCredentials != null)
				return false;
		} else if (!userCredentials.equals(other.userCredentials))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
