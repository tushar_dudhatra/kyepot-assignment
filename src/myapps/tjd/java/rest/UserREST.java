/**
 * 
 */
package myapps.tjd.java.rest;

import myapps.tjd.java.mgr.UserMgr;
import myapps.tjd.java.model.VUser;
import myapps.tjd.java.model.VUserCredentials;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author tdudhatra
 *
 */

@Component
@Path("/user")
@Scope("request")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserREST {

	@Autowired
	UserMgr userMgr;

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VUser createUser(VUser vUser) throws Exception {
		userMgr.authenticate(vUser.getUserCredentials());
		return userMgr.createUser(vUser);
	}

	@POST
	@Path("/enquire/userId/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public VUser getUserDetailById(@PathParam("userId") Long userId, VUserCredentials userCredentials) throws Exception {
		userMgr.authenticate(userCredentials);
		return userMgr.getUserDetailById(userId);
	}

	@POST
	@Path("/enquire/accountNo/{accountNo}/emailId/{emailId}")
	@Produces(MediaType.APPLICATION_JSON)
	public VUser getUserDetailByAccountNo(@PathParam("accountNo") Long accountNo, @PathParam("emailId") String emailId, VUserCredentials userCredentials) throws Exception {
		userMgr.authenticate(userCredentials);
		return userMgr.getUserDetailByAccountNoAndEmailId(accountNo, emailId);
	}

}
