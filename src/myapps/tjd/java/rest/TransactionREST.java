/**
 * 
 */
package myapps.tjd.java.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import myapps.tjd.java.mgr.TransactionMgr;
import myapps.tjd.java.mgr.UserMgr;
import myapps.tjd.java.model.VTransaction;

/**
 * @author tdudhatra
 *
 */

@Component
@Path("/transaction")
@Scope("request")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class TransactionREST {

	@Autowired
	TransactionMgr	transactionMgr;

	@Autowired
	UserMgr			userMgr;

	@POST
	@Path("/doTransaction")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VTransaction doTransaction(VTransaction vTransaction) throws Exception {

		if (vTransaction == null) {
			throw new Exception("Invalid Input Data");
		}

		userMgr.authenticate(vTransaction.getUserCredentials());
		return transactionMgr.doTransaction(vTransaction);
	}

}
