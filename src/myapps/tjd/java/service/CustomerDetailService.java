/**
 * 
 */
package myapps.tjd.java.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import myapps.tjd.db.entity.KPCustomerDetail;
import myapps.tjd.java.model.VCustomerDetail;

/**
 * @author tdudhatra
 *
 */
@Service
@Scope("singleton")
public class CustomerDetailService extends KPServiceBase1<KPCustomerDetail, VCustomerDetail> {

	@Override
	protected KPCustomerDetail viewToEntity(KPCustomerDetail dbObj, VCustomerDetail vObj, int operationContext) {
		dbObj.setUserId(vObj.getUserId());
		dbObj.setAccountNo(vObj.getAccountNo());
		dbObj.setAddress(vObj.getAddress());
		dbObj.setContactNo(vObj.getContactNo());
		return dbObj;
	}

	@Override
	protected VCustomerDetail entityToView(KPCustomerDetail dbObj, VCustomerDetail vObj) {
		vObj.setUserId(dbObj.getUserId());
		vObj.setAccountNo(dbObj.getAccountNo());
		vObj.setAddress(dbObj.getAddress());
		vObj.setContactNo(dbObj.getContactNo());
		return vObj;
	}

	@Override
	public VCustomerDetail getPopulatedViewObject(KPCustomerDetail dbObj) {
		return entityToViewMapping(dbObj);
	}

}
