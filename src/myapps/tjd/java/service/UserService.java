/**
 * 
 */
package myapps.tjd.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import myapps.tjd.db.entity.KPCustomerDetail;
import myapps.tjd.db.entity.KPUser;
import myapps.tjd.java.model.VUser;

/**
 * @author tdudhatra
 *
 */
@Service
@Scope("singleton")
public class UserService extends KPServiceBase0<KPUser, VUser> {

	@Autowired
	CustomerDetailService customerDetailService;

	@Override
	protected KPUser viewToEntity(KPUser dbObj, VUser vObj, int operationContext) {
		dbObj.setFirstName(vObj.getFirstName());
		dbObj.setLastName(vObj.getLastName());
		dbObj.setEmailId(vObj.getEmailId());
		dbObj.setPassword(vObj.getPassword());
		return dbObj;
	}

	@Override
	protected VUser entityToView(KPUser dbObj, VUser vObj) {
		vObj.setFirstName(dbObj.getFirstName());
		vObj.setLastName(dbObj.getLastName());
		vObj.setEmailId(dbObj.getEmailId());
		vObj.setPassword(dbObj.getPassword());

		List<String> userRoles = daoHandler.getKPUserRole().findRolesByUserId(dbObj.getId());
		vObj.setUserRoles(userRoles);

		KPCustomerDetail customerDetail = daoHandler.getKPCustomerDetail().findByUserId(dbObj.getId());
		if (customerDetail != null) {
			vObj.setCustomerDetail(customerDetailService.getPopulatedViewObject(customerDetail));
		}

		return vObj;
	}

	@Override
	public VUser getPopulatedViewObject(KPUser dbObj) {
		return entityToViewMapping(dbObj);
	}

}
