/**
 * 
 */
package myapps.tjd.java.service;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import myapps.tjd.common.KPCommonUtil;
import myapps.tjd.common.KPConstants;
import myapps.tjd.db.dao.KPDaoBase;
import myapps.tjd.db.dao.KPDaoHandler;
import myapps.tjd.db.entity.KPEntityBase0;
import myapps.tjd.java.model.KPViewBase0;

/**
 * @author tdudhatra
 *
 */
public abstract class KPServiceBase0<D extends KPEntityBase0, V extends KPViewBase0> {

	@Autowired
	KPDaoHandler		daoHandler;

	protected Class<D>	dbClass;
	protected Class<V>	vClass;

	protected abstract D viewToEntity(D dbObj, V vObj, int operationContext);

	protected abstract V entityToView(D dbObj, V vObj);

	public abstract V getPopulatedViewObject(D dbObj);

	@SuppressWarnings("unchecked")
	public KPServiceBase0() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();

		Type type = genericSuperclass.getActualTypeArguments()[0];
		Type type2 = genericSuperclass.getActualTypeArguments()[1];

		if (type instanceof ParameterizedType) {
			this.dbClass = (Class<D>) ((ParameterizedType) type).getRawType();
		} else {
			this.dbClass = (Class<D>) type;
		}
		if (type2 instanceof ParameterizedType) {
			this.vClass = (Class<V>) ((ParameterizedType) type2).getRawType();
		} else {
			this.vClass = (Class<V>) type2;
		}

	}

	protected D viewToEntityMapping(V vObj, int operationContext) throws Exception {
		D dbObj = null;

		if (operationContext == KPConstants.OPERATION_CONTEXT_CREATE) {
			dbObj = getNewEntityObject();
			dbObj.setCreateTime(new Date());
			dbObj.setUpdateTime(new Date());
			dbObj.setGuId(generateGlobalId());
		} else if (operationContext == KPConstants.OPERATION_CONTEXT_UPDATE) {
			dbObj = getDao().read(vObj.getId());

			if (dbObj == null) {
				throw new Exception("Object that you want to update does not exists.");
			}

			if (dbObj.getCreateTime() == null) {
				dbObj.setCreateTime(new Date());
			}
			if (dbObj.getGuId() == null) {
				dbObj.setGuId(generateGlobalId());
			}
			dbObj.setUpdateTime(new Date());
		}

		return viewToEntity(dbObj, vObj, operationContext);
	}

	protected V entityToViewMapping(D dbObj) {
		V vObj = getNewViewObject();

		vObj.setId(dbObj.getId());
		vObj.setCreateTime(dbObj.getCreateTime());
		vObj.setUpdateTime(dbObj.getUpdateTime());
		vObj.setGuid(dbObj.getGuId());
		return entityToView(dbObj, vObj);
	}

	private D getNewEntityObject() {
		try {
			return dbClass.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private V getNewViewObject() {
		try {
			return vClass.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String generateGlobalId() {
		return KPCommonUtil.genGUI();
	}

	@SuppressWarnings("unchecked")
	public KPDaoBase<D> getDao() {
		return (KPDaoBase<D>) daoHandler.getDaoFromClassName(dbClass.getSimpleName());
	}

	public V readObject(Long id) throws Exception {
		D dbObj = getDao().read(id);
		if (dbObj == null) {
			throw new Exception("Data not found with ID: " + id);
		}
		return entityToViewMapping(dbObj);
	}

	public V createObject(V vObj) throws Exception {
		D dbObj = preCreate(vObj);
		dbObj = getDao().write(dbObj);
		return postCreate(dbObj);
	}

	protected D preCreate(V vObj) throws Exception {
		D d = getNewEntityObject();
		d = viewToEntityMapping(vObj, KPConstants.OPERATION_CONTEXT_CREATE);
		return d;
	}

	protected V postCreate(D dbObj) {
		V vObj = entityToViewMapping(dbObj);
		return vObj;
	}

	public V updateObject(V vObj) {
		D resource = preUpdate(vObj);
		resource = getDao().update(resource);
		V viewBean = postUpdate(resource);
		return viewBean;
	}

	protected D preUpdate(V viewBaseBean) {
		D resource = getDao().read(viewBaseBean.getId());
		try {
			if (resource == null) {
				throw new Exception("No Object found to update !");
			}
			return viewToEntityMapping(viewBaseBean, KPConstants.OPERATION_CONTEXT_UPDATE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected V postUpdate(D dbObj) {
		V vObj = entityToViewMapping(dbObj);
		return vObj;
	}

	public boolean deleteObject(Long id) {
		D resource = preDelete(id);
		boolean result = false;
		try {
			if (resource == null) {
				throw new Exception("No Object found to delete !");
			}
			result = getDao().remove(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	protected D preDelete(Long id) {
		D resource = getDao().read(id);
		return resource;
	}

}
