/**
 * 
 */
package myapps.tjd.java.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import myapps.tjd.db.entity.KPUserRole;
import myapps.tjd.java.model.VUserRole;

/**
 * @author tdudhatra
 *
 */
@Service
@Scope("singleton")
public class UserRoleService extends KPServiceBase0<KPUserRole, VUserRole> {

	@Override
	protected KPUserRole viewToEntity(KPUserRole dbObj, VUserRole vObj, int operationContext) {
		dbObj.setUserId(vObj.getUserId());
		dbObj.setUserRole(vObj.getUserRole());
		return dbObj;
	}

	@Override
	protected VUserRole entityToView(KPUserRole dbObj, VUserRole vObj) {
		vObj.setUserId(dbObj.getUserId());
		vObj.setUserRole(dbObj.getUserRole());
		return vObj;
	}

	@Override
	public VUserRole getPopulatedViewObject(KPUserRole dbObj) {
		return entityToViewMapping(dbObj);
	}

}
