/**
 * 
 */
package myapps.tjd.java.service;

import myapps.tjd.db.entity.KPEntityBase1;
import myapps.tjd.java.model.KPViewBase1;

/**
 * @author tdudhatra
 *
 */
public abstract class KPServiceBase1<D extends KPEntityBase1, V extends KPViewBase1> extends KPServiceBase0<D, V> {

	public static final String	DUMMY_USER_NAME	= "dummy_user";

	@Override
	protected V entityToViewMapping(D dbObj) {
		V viewObj = super.entityToViewMapping(dbObj);

		viewObj.setCreatedBy(DUMMY_USER_NAME);
		viewObj.setUpdatedBy(DUMMY_USER_NAME);
		return viewObj;
	}

	@Override
	protected D viewToEntityMapping(V vObj, int operationContext) throws Exception {

		D dbObj = super.viewToEntityMapping(vObj, operationContext);

		if (dbObj.getAddedById() == null) {
			dbObj.setAddedById(1L); // Ideally this should be populated from Logged In user-session
		}
		dbObj.setUpdatedById(1L); // Ideally this should be populated from Logged In user-session

		return dbObj;
	}

}
