package myapps.tjd.java.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import myapps.tjd.db.entity.KPTransaction;
import myapps.tjd.java.model.VTransaction;

/**
 * @author tdudhatra
 *
 */
@Service
@Scope("singleton")
public class TransactionService extends KPServiceBase1<KPTransaction, VTransaction> {

	@Override
	protected KPTransaction viewToEntity(KPTransaction dbObj, VTransaction vObj, int operationContext) {
		dbObj.setUserId(vObj.getUserId());
		dbObj.setTransactionType(vObj.getTransactionType());
		dbObj.setTransactionMode(vObj.getTransactionMode());
		dbObj.setTransactionAmt(vObj.getTransactionAmt());
		dbObj.setClosingBalance(vObj.getClosingBalance());
		return dbObj;
	}

	@Override
	protected VTransaction entityToView(KPTransaction dbObj, VTransaction vObj) {
		vObj.setUserId(dbObj.getUserId());
		vObj.setTransactionType(dbObj.getTransactionType());
		vObj.setTransactionMode(dbObj.getTransactionMode());
		vObj.setTransactionAmt(dbObj.getTransactionAmt());
		vObj.setClosingBalance(dbObj.getClosingBalance());
		return vObj;
	}

	@Override
	public VTransaction getPopulatedViewObject(KPTransaction dbObj) {
		return entityToViewMapping(dbObj);
	}

}
