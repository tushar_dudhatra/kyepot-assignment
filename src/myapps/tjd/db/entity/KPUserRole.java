/**
 * 
 */
package myapps.tjd.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import myapps.tjd.common.KPConstants;

/**
 * @author tdudhatra
 *
 */
@Entity
@XmlRootElement
@Table(name = "kp_user_role")
public class KPUserRole extends KPEntityBase0 implements java.io.Serializable {
	private static final long	serialVersionUID	= 1L;

	@Column(name = "user_id")
	protected Long				userId;

	@Column(name = "user_role")
	protected String			userRole;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public Integer getMyClassType() {
		return KPConstants.CLASS_TYPE_USER_ROLE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userRole == null) ? 0 : userRole.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KPUserRole other = (KPUserRole) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userRole == null) {
			if (other.userRole != null)
				return false;
		} else if (!userRole.equals(other.userRole))
			return false;
		return true;
	}

}
