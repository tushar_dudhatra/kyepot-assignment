/**
 * 
 */
package myapps.tjd.db.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import myapps.tjd.common.KPConstants;

/**
 * @author tdudhatra
 *
 */
@Entity
@XmlRootElement
@Table(name = "kp_transaction")
public class KPTransaction extends KPEntityBase1 implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@Column(name = "user_id")
	protected Long				userId;

	@Column(name = "transaction_type")
	protected int				transactionType;

	@Column(name = "transaction_mode")
	protected int				transactionMode;

	@Column(name = "transaction_amt")
	protected double			transactionAmt;

	@Column(name = "closing_balance")
	protected double			closingBalance;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public int getTransactionMode() {
		return transactionMode;
	}

	public void setTransactionMode(int transactionMode) {
		this.transactionMode = transactionMode;
	}

	public double getTransactionAmt() {
		return transactionAmt;
	}

	public void setTransactionAmt(double transactionAmt) {
		this.transactionAmt = transactionAmt;
	}

	public double getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(double closingBalance) {
		this.closingBalance = closingBalance;
	}

	public Integer getMyClassType() {
		return KPConstants.CLASS_TYPE_TRANSACTION;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(closingBalance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(transactionAmt);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + transactionMode;
		result = prime * result + transactionType;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KPTransaction other = (KPTransaction) obj;
		if (Double.doubleToLongBits(closingBalance) != Double.doubleToLongBits(other.closingBalance))
			return false;
		if (Double.doubleToLongBits(transactionAmt) != Double.doubleToLongBits(other.transactionAmt))
			return false;
		if (transactionMode != other.transactionMode)
			return false;
		if (transactionType != other.transactionType)
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
