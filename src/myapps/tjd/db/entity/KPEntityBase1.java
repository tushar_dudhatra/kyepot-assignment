/**
 * 
 */
package myapps.tjd.db.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlRootElement;

import myapps.tjd.common.KPConstants;

/**
 * @author tdudhatra
 *
 */

@MappedSuperclass
@XmlRootElement
public class KPEntityBase1 extends KPEntityBase0 implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@Column(name = "ADDED_BY_ID")
	private Long				addedById;

	@Column(name = "UPD_BY_ID")
	private Long				updatedById;

	public KPEntityBase1() {

	}

	public Long getAddedById() {
		return addedById;
	}

	public void setAddedById(Long addedById) {
		this.addedById = addedById;
	}

	public Long getUpdatedById() {
		return updatedById;
	}

	public void setUpdatedById(Long updatedById) {
		this.updatedById = updatedById;
	}

	@Override
	public Integer getMyClassType() {
		return KPConstants.CLASS_TYPE_OBJ_BASE_1;
	}

}
