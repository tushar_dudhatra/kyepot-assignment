/**
 * 
 */
package myapps.tjd.db.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import myapps.tjd.common.KPConstants;

/**
 * @author tdudhatra
 *
 */

@MappedSuperclass
@XmlRootElement
public class KPEntityBase0 implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long				id;

	@Column(name = "GU_ID", nullable = false)
	private String				guid;

	@Column(name = "CREATE_TIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date				createTime;

	@Column(name = "UPDATE_TIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date				updateTime;

	@Column(name = "obj_status", nullable = false)
	private int					objStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGuId() {
		return guid;
	}

	public void setGuId(String guId) {
		this.guid = guId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getObjStatus() {
		return objStatus;
	}

	public void setObjStatus(int objStatus) {
		this.objStatus = objStatus;
	}

	public Integer getMyClassType() {
		return KPConstants.CLASS_TYPE_NONE;
	}

}