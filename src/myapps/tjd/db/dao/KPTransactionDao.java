/**
 * 
 */
package myapps.tjd.db.dao;

import javax.persistence.NoResultException;

import myapps.tjd.db.entity.KPTransaction;

/**
 * @author tdudhatra
 *
 */
public class KPTransactionDao extends KPDaoBase<KPTransaction> {

	public KPTransactionDao(KPDaoHandler daoMgr) {
		super(daoMgr);
	}

	public KPTransaction findLastTransactionOfUser(Long userId) {
		if (userId == null) {
			return null;
		}
		try {
			return getEntityManager().createNamedQuery("KPTransaction.findLastTransactionOfUser", dbClass).setParameter("userId", userId).setMaxResults(1).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
