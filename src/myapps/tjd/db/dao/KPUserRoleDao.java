/**
 * 
 */
package myapps.tjd.db.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import myapps.tjd.db.entity.KPUserRole;

/**
 * @author tdudhatra
 *
 */
public class KPUserRoleDao extends KPDaoBase<KPUserRole> {

	public KPUserRoleDao(KPDaoHandler daoMgr) {
		super(daoMgr);
	}

	@SuppressWarnings("unchecked")
	public List<String> findRolesByUserId(Long userId) {

		if (userId == null) {
			return new ArrayList<>();
		}
		try {
			return getEntityManager().createNamedQuery("KPUserRole.findRolesByUserId").setParameter("userId", userId).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}

	}

}
