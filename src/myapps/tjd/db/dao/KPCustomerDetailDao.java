/**
 * 
 */
package myapps.tjd.db.dao;

import javax.persistence.NoResultException;

import myapps.tjd.db.entity.KPCustomerDetail;

/**
 * @author tdudhatra
 *
 */
public class KPCustomerDetailDao extends KPDaoBase<KPCustomerDetail> {

	public KPCustomerDetailDao(KPDaoHandler daoMgr) {
		super(daoMgr);
	}

	public KPCustomerDetail findLastCreatedCustomer() {
		try {
			return getEntityManager().createNamedQuery("KPCustomerDetail.findLastCreatedCustomer", KPCustomerDetail.class).setMaxResults(1).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public KPCustomerDetail findByUserId(Long userId) {
		if (userId == null) {
			return null;
		}
		try {
			return getEntityManager().createNamedQuery("KPCustomerDetail.findByUserId", dbClass).setParameter("userId", userId).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
