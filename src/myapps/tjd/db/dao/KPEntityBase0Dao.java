/**
 * 
 */
package myapps.tjd.db.dao;

import myapps.tjd.db.entity.KPEntityBase0;

/**
 * @author tdudhatra
 *
 */
public class KPEntityBase0Dao extends KPDaoBase<KPEntityBase0> {

	public KPEntityBase0Dao(KPDaoHandler daoHandler) {
		super(daoHandler);
	}
}
