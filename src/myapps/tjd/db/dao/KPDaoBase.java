/**
 * 
 */
package myapps.tjd.db.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * @author tdudhatra
 *
 */

public class KPDaoBase<D> {

	protected KPDaoHandler	daoMgr;
	EntityManager			em;

	protected Class<D>		dbClass;

	@SuppressWarnings("unchecked")
	public KPDaoBase(KPDaoHandler daoMgr) {

		this.daoMgr = daoMgr;
		this.em = daoMgr.getEntityManager();

		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();

		Type type = genericSuperclass.getActualTypeArguments()[0];

		if (type instanceof ParameterizedType) {
			this.dbClass = (Class<D>) ((ParameterizedType) type).getRawType();
		} else {
			this.dbClass = (Class<D>) type;
		}
	}

	public D write(D d) {
		if (d == null) {
			return null;
		}
		try {
			em.persist(d);
			em.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return d;
	}

	public D update(D d) {
		if (d == null) {
			return null;
		}
		try {
			em.merge(d);
			em.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return d;
	}

	public D read(Long objId) {
		if (objId == null) {
			return null;
		}
		D d = null;
		try {
			d = em.find(dbClass, objId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return d;
	}

	public boolean remove(Long objId) {
		boolean retResult = false;
		try {
			D d = read(objId);
			if (d == null) {
				return true;
			}
			em.remove(d);
			em.flush();
			retResult = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return retResult;
	}

	public List<D> getAll() {
		List<D> ret = null;
		TypedQuery<D> qry = em.createQuery("SELECT d FROM " + dbClass.getSimpleName() + " d", dbClass);
		ret = qry.getResultList();
		return ret;
	}

	public EntityManager getEntityManager() {
		return em;
	}

}
