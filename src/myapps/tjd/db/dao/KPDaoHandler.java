/** 
 * 
 */
package myapps.tjd.db.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

/**
 * @author tdudhatra
 *
 */

@Component
public class KPDaoHandler {

	@PersistenceContext(unitName = "persistenceUnit")
	private EntityManager em;

	public EntityManager getEntityManager() {
		return em;
	}

	public KPEntityBase0Dao getKPEntityBase0() {
		return new KPEntityBase0Dao(this);
	}

	public KPEntityBase1Dao getKPEntityBase1() {
		return new KPEntityBase1Dao(this);
	}

	public KPUserDao getKPUser() {
		return new KPUserDao(this);
	}

	public KPUserRoleDao getKPUserRole() {
		return new KPUserRoleDao(this);
	}

	public KPCustomerDetailDao getKPCustomerDetail() {
		return new KPCustomerDetailDao(this);
	}

	public KPTransactionDao getKPTransaction() {
		return new KPTransactionDao(this);
	}

	public KPDaoBase<?> getDaoFromClassName(String clsName) {

		switch (clsName) {
		case "KPEntityBase0":
			return getKPEntityBase0();
		case "KPEntityBase1":
			return getKPEntityBase1();
		case "KPUser":
			return getKPUser();
		case "KPUserRole":
			return getKPUserRole();
		case "KPCustomerDetail":
			return getKPCustomerDetail();
		case "KPTransaction":
			return getKPTransaction();
		default:
			return null;
		}
	}

}
