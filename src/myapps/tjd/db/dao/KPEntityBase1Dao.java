/**
 * 
 */
package myapps.tjd.db.dao;

import myapps.tjd.db.entity.KPEntityBase1;

/**
 * @author tdudhatra
 *
 */
public class KPEntityBase1Dao extends KPDaoBase<KPEntityBase1> {

	public KPEntityBase1Dao(KPDaoHandler daoHandler) {
		super(daoHandler);
	}
}
