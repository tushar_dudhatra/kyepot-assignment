/**
 * 
 */
package myapps.tjd.db.dao;

import javax.persistence.NoResultException;

import org.springframework.util.StringUtils;

import myapps.tjd.db.entity.KPUser;

/**
 * @author tdudhatra
 *
 */
public class KPUserDao extends KPDaoBase<KPUser> {

	public KPUserDao(KPDaoHandler daoMgr) {
		super(daoMgr);
	}

	public KPUser findByEmailIdAndAccountNo(String emailId, Long acNo) {
		if (StringUtils.isEmpty(emailId) || acNo == null) {
			return null;
		}
		try {
			return getEntityManager().createNamedQuery("KPUser.findByEmailIdAndAccountNo", dbClass).setParameter("emailId", emailId).setParameter("acNo", acNo).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
