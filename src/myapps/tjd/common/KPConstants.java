/**
 * 
 */
package myapps.tjd.common;

import java.util.HashMap;

/**
 * @author tdudhatra
 *
 */
public class KPConstants {

	public static HashMap<Integer, String>	enumNlabel;
	public static final int					UNKNOWN_CONSTANT			= -1;
	public static final int					HIGHEST_VALUE				= 9;

	public static final Long				ACCOUNT_NO_STARTING_POINT	= 500000000000L;

	/***************************************************************
	 * Constant values for ClassTypes, Value Starts from : 1000
	 **************************************************************/
	public static final int					CLASS_TYPE_NONE				= 1000;
	public static final int					CLASS_TYPE_OBJ_BASE_1		= 1001;
	public static final int					CLASS_TYPE_USER				= 1002;
	public static final int					CLASS_TYPE_USER_ROLE		= 1003;
	public static final int					CLASS_TYPE_CUSTOMER_DETAIL	= 1004;
	public static final int					CLASS_TYPE_TRANSACTION		= 1005;

	/***************************************************************
	 * ConstantType : OperationContext, Value Starts from : 1
	 **************************************************************/
	public static final int					OPERATION_CONTEXT_UNKNOWN	= 1;
	public static final int					OPERATION_CONTEXT_CREATE	= 2;
	public static final int					OPERATION_CONTEXT_UPDATE	= 3;
	public static final int					OPERATION_CONTEXT_READ		= 4;
	public static final int					OPERATION_CONTEXT_DELETE	= 5;

	/***************************************************************
	 * Constant values for TransactionType, Value Starts from : 6
	 **************************************************************/
	public static final int					TRANSACTION_TYPE_DEBIT		= 6;
	public static final int					TRANSACTION_TYPE_CREDIT		= 7;

	/***************************************************************
	 * Constant values for TransactionMode, Value Starts from : 8
	 **************************************************************/
	public static final int					TRANSACTION_MODE_CASH		= 8;
	public static final int					TRANSACTION_MODE_CHEQUE		= 9;

}
