/**
 * 
 */
package myapps.tjd.common;

import java.util.HashMap;

import org.springframework.stereotype.Component;

/**
 * @author tdudhatra
 *
 */

@Component
public class KPConstantsUtil extends KPConstants {

	public static HashMap<Integer, String> enumNlabel;

	public void init() {
		enumNlabel.put(UNKNOWN_CONSTANT, "unknownConstant");

		// *****************************************

		enumNlabel.put(OPERATION_CONTEXT_UNKNOWN, "operationUnknown");
		enumNlabel.put(OPERATION_CONTEXT_CREATE, "operationCreate");
		enumNlabel.put(OPERATION_CONTEXT_UPDATE, "operationUpdate");
		enumNlabel.put(OPERATION_CONTEXT_READ, "operationRead");
		enumNlabel.put(OPERATION_CONTEXT_DELETE, "operationDelete");

		// *****************************************

		enumNlabel.put(CLASS_TYPE_NONE, "CLASS_TYPE_NONE");
		enumNlabel.put(CLASS_TYPE_OBJ_BASE_1, "CLASS_TYPE_OBJ_BASE_1");
		enumNlabel.put(CLASS_TYPE_USER, "CLASS_TYPE_USER");
		enumNlabel.put(CLASS_TYPE_USER_ROLE, "CLASS_TYPE_USER_ROLE");
		enumNlabel.put(CLASS_TYPE_CUSTOMER_DETAIL, "CLASS_TYPE_CUSTOMER_DETAIL");
		enumNlabel.put(CLASS_TYPE_TRANSACTION, "CLASS_TYPE_TRANSACTION");

		// *****************************************

		enumNlabel.put(TRANSACTION_MODE_CASH, "Cash");
		enumNlabel.put(TRANSACTION_MODE_CHEQUE, "Cheque");

		// *****************************************

		enumNlabel.put(TRANSACTION_TYPE_DEBIT, "Debit");
		enumNlabel.put(TRANSACTION_TYPE_CREDIT, "Credit");

	}

	/*
	 * 
	 * GetLabel function that will return Label for given EnumValue
	 */
	public String getLabelForConstants(int enumVal) {

		if (enumNlabel == null) {
			enumNlabel = new HashMap<>();
			init();
		}
		String label = enumNlabel.get(UNKNOWN_CONSTANT);

		switch (enumVal) {

		case OPERATION_CONTEXT_UNKNOWN:
			label = enumNlabel.get(OPERATION_CONTEXT_UNKNOWN);
			break;
		case OPERATION_CONTEXT_CREATE:
			label = enumNlabel.get(OPERATION_CONTEXT_CREATE);
			break;
		case OPERATION_CONTEXT_UPDATE:
			label = enumNlabel.get(OPERATION_CONTEXT_UPDATE);
			break;
		case OPERATION_CONTEXT_READ:
			label = enumNlabel.get(OPERATION_CONTEXT_READ);
			break;
		case OPERATION_CONTEXT_DELETE:
			label = enumNlabel.get(OPERATION_CONTEXT_DELETE);
			break;

		// *****************************************

		case CLASS_TYPE_NONE:
			label = enumNlabel.get(CLASS_TYPE_NONE);
			break;
		case CLASS_TYPE_OBJ_BASE_1:
			label = enumNlabel.get(CLASS_TYPE_OBJ_BASE_1);
			break;
		case CLASS_TYPE_USER:
			label = enumNlabel.get(CLASS_TYPE_USER);
			break;
		case CLASS_TYPE_USER_ROLE:
			label = enumNlabel.get(CLASS_TYPE_USER_ROLE);
			break;
		case CLASS_TYPE_CUSTOMER_DETAIL:
			label = enumNlabel.get(CLASS_TYPE_CUSTOMER_DETAIL);
			break;
		case CLASS_TYPE_TRANSACTION:
			label = enumNlabel.get(CLASS_TYPE_TRANSACTION);
			break;

		// *****************************************

		case TRANSACTION_TYPE_DEBIT:
			label = enumNlabel.get(TRANSACTION_TYPE_DEBIT);
			break;
		case TRANSACTION_TYPE_CREDIT:
			label = enumNlabel.get(TRANSACTION_TYPE_CREDIT);
			break;

		// *****************************************

		case TRANSACTION_MODE_CASH:
			label = enumNlabel.get(TRANSACTION_MODE_CASH);
			break;
		case TRANSACTION_MODE_CHEQUE:
			label = enumNlabel.get(TRANSACTION_MODE_CHEQUE);
			break;

		default:
			break;
		}
		return label;
	}

	public int getEnumForConstants(String label) {
		int enumVal = UNKNOWN_CONSTANT;

		if (getLabelForConstants(OPERATION_CONTEXT_UNKNOWN).equalsIgnoreCase(label)) {
			return OPERATION_CONTEXT_UNKNOWN;
		}

		// *****************************************

		if (getLabelForConstants(OPERATION_CONTEXT_CREATE).equalsIgnoreCase(label)) {
			return OPERATION_CONTEXT_CREATE;
		}
		if (getLabelForConstants(OPERATION_CONTEXT_UPDATE).equalsIgnoreCase(label)) {
			return OPERATION_CONTEXT_UPDATE;
		}
		if (getLabelForConstants(OPERATION_CONTEXT_READ).equalsIgnoreCase(label)) {
			return OPERATION_CONTEXT_READ;
		}
		if (getLabelForConstants(OPERATION_CONTEXT_DELETE).equalsIgnoreCase(label)) {
			return OPERATION_CONTEXT_DELETE;
		}

		// *****************************************

		if (getLabelForConstants(CLASS_TYPE_NONE).equalsIgnoreCase(label)) {
			return CLASS_TYPE_NONE;
		}
		if (getLabelForConstants(CLASS_TYPE_OBJ_BASE_1).equalsIgnoreCase(label)) {
			return CLASS_TYPE_OBJ_BASE_1;
		}
		if (getLabelForConstants(CLASS_TYPE_USER).equalsIgnoreCase(label)) {
			return CLASS_TYPE_USER;
		}
		if (getLabelForConstants(CLASS_TYPE_USER_ROLE).equalsIgnoreCase(label)) {
			return CLASS_TYPE_USER_ROLE;
		}
		if (getLabelForConstants(CLASS_TYPE_CUSTOMER_DETAIL).equalsIgnoreCase(label)) {
			return CLASS_TYPE_CUSTOMER_DETAIL;
		}
		if (getLabelForConstants(CLASS_TYPE_TRANSACTION).equalsIgnoreCase(label)) {
			return CLASS_TYPE_OBJ_BASE_1;
		}

		// *****************************************

		if (getLabelForConstants(TRANSACTION_MODE_CASH).equalsIgnoreCase(label)) {
			return TRANSACTION_MODE_CASH;
		}
		if (getLabelForConstants(TRANSACTION_MODE_CHEQUE).equalsIgnoreCase(label)) {
			return TRANSACTION_MODE_CHEQUE;
		}

		// *****************************************

		if (getLabelForConstants(TRANSACTION_TYPE_DEBIT).equalsIgnoreCase(label)) {
			return TRANSACTION_TYPE_DEBIT;
		}
		if (getLabelForConstants(TRANSACTION_TYPE_CREDIT).equalsIgnoreCase(label)) {
			return TRANSACTION_TYPE_CREDIT;
		}

		return enumVal;
	}

}
