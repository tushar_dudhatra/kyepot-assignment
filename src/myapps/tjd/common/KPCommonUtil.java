/**
 * 
 */
package myapps.tjd.common;

import java.security.SecureRandom;

/**
 * @author tdudhatra
 *
 */

public class KPCommonUtil {

	static SecureRandom	secureRandom	= new SecureRandom();
	static int			counter			= 0;

	static public String genGUI() {
		return System.currentTimeMillis() + "_" + secureRandom.nextInt(1000) + "_" + counter++;
	}

}
