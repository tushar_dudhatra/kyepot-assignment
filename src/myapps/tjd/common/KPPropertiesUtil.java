/**
 * 
 */
package myapps.tjd.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * @author tdudhatra
 *
 */
public class KPPropertiesUtil extends PropertyPlaceholderConfigurer {

	private static Map<String, String> propertiesMap = new HashMap<String, String>();

	private KPPropertiesUtil() {

	}

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) throws BeansException {

		super.processProperties(beanFactory, props);

		// add the system properties first
		Set<Object> keySet = System.getProperties().keySet();
		for (Object key : keySet) {
			String keyStr = key.toString();
			propertiesMap.put(keyStr, System.getProperties().getProperty(keyStr).trim());
		}

		// add our properties now which's defined in
		// KPSystemProperties.properties
		keySet = props.keySet();
		for (Object key : keySet) {
			String keyStr = key.toString();
			propertiesMap.put(keyStr, props.getProperty(keyStr).trim());
		}
	}

	public static String getProperty(String key, String defaultValue) {
		if (key == null) {
			return null;
		}
		String rtrnVal = propertiesMap.get(key);
		if (rtrnVal == null) {
			rtrnVal = defaultValue;
		}
		return rtrnVal;
	}

	public static String[] getPropertyAsStringArray(String key) {
		if (key == null) {
			return null;
		}
		String value = propertiesMap.get(key);
		if (value != null) {
			String[] splitValues = value.split(",");
			String[] returnValues = new String[splitValues.length];
			for (int i = 0; i < splitValues.length; i++) {
				returnValues[i] = splitValues[i].trim();
			}
			return returnValues;
		} else {
			return new String[0];
		}
	}

}
